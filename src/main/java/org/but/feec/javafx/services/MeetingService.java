package org.but.feec.javafx.services;

import org.but.feec.javafx.api.MeetingView;
import org.but.feec.javafx.data.MeetingRepository;

import java.util.List;

public class MeetingService {
    private MeetingRepository meetingRepository;

    public MeetingService(MeetingRepository meetingRepository) {
        this.meetingRepository = meetingRepository;
    }

    public MeetingView findById(Long id) {
        MeetingView meetingView = meetingRepository.findById(id);
        return meetingView;
    }

    public List<MeetingView> findAll() {
        List<MeetingView> meetingsView = meetingRepository.findAll();
        return meetingsView;
    }
}
