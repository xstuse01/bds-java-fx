package org.but.feec.javafx.data;

import org.but.feec.javafx.api.MeetingView;
import org.but.feec.javafx.config.DataSourceConfig;
import org.but.feec.javafx.exceptions.DataAccessException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MeetingRepository {

    public MeetingView findById(Long id) {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT id_meeting, note, place, start_time, end_time FROM bds.meeting WHERE id_meeting = ?")
        ) {
            preparedStatement.setLong(1, id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return mapToMeeting(resultSet);
                }
            }
        } catch (SQLException e) {
            throw new DataAccessException("Find meetings by ID SQL failed.", e);
        }
        return null;
    }

    public List<MeetingView> findAll() {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT id_meeting, note, place, start_time, end_time FROM bds.meeting");
             ResultSet resultSet = preparedStatement.executeQuery()) {
            List<MeetingView> meetings = new ArrayList<>();
            while (resultSet.next()) {
                meetings.add(mapToMeeting(resultSet));
            }
            return meetings;
        } catch (SQLException e) {
            throw new DataAccessException("Find all meetings SQL failed.", e);
        }
    }

    private MeetingView mapToMeeting(ResultSet rs) throws SQLException {
        MeetingView m = new MeetingView();
        m.setIdMeeting(rs.getLong("id_meeting"));
        m.setNote(rs.getString("note"));
        m.setPlace(rs.getString("place"));
        m.setStartTime(rs.getDate("start_time").toLocalDate());
        m.setEndTime(rs.getDate("end_time").toLocalDate());
        return m;
    }
}
